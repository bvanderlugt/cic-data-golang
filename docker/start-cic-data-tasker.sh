#!/bin/sh

set -e


if [ -n "$PG_HOST" ];then
./docker/wait-for-it.sh "$PG_HOST:$PG_PORT"
fi

./scripts/db.sh

./cic-data-tasker

