package job

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/httputil"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/server"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"

	"github.com/pkg/errors"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/encrypt"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/sftp"

	"github.com/go-co-op/gocron"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/service"
)

var (
	userSvc        *service.UserService
	transactionSvc *service.TransactionService
	s              *gocron.Scheduler
	web3Util       *web3.Web3
)

func Init() {
	var err error
	web3Util, err = web3.NewWeb3(httputil.NewDefaultClient(), &web3.Conf{
		config.Conf.Web3Provider.Host,
		config.Conf.Web3Provider.Port,
		config.Conf.DataProvider.UserHost,
		config.Conf.DataProvider.UserPort,
		config.Conf.Web3Provider.AccountRegistryAddress,
		config.Conf.Web3Provider.TokenContractAddress,
	})
	if err != nil {
		log.Fatalf("cannot connect to web3: %v", err)
	}
	logger, err := server.NewZapLogger()
	if err != nil {
		log.Fatalf("cannot create logger: %v \n", err)
	}
	userSvc = &service.UserService{
		PG:     &dao.UserPG{},
		Web3:   web3Util,
		Logger: logger,
	}
	transactionSvc = &service.TransactionService{
		PG:     &dao.TransactionPG{},
		Logger: logger,
	}
	s = gocron.NewScheduler(time.UTC)
	s.Every(1).Minute().SingletonMode().Do(generateCsv)
	s.StartAsync()
}

func generateCsv() {
	updateAllUsers()
	generateUsersCsv()
	generateTransactionsCsv()
}

func updateAllUsers() {
	log.Println("Update All Users...")
	userCollections, err := getUserCollections()
	if err != nil {
		log.Println(errors.Errorf("Job -> bootstrapping Error -> %s", err))
		return
	}

	err = userSvc.ProcessAllUser(userCollections)
	if err != nil {
		log.Println(errors.Errorf("Job -> bootstrapping Error -> %s", err))
		return
	}
	log.Println("Update All Users Done...")
}

func generateUsersCsv() {
	log.Println("Generating Users CSV...")

	users, err := userSvc.GetAllUser()
	if err != nil {
		log.Println(err.Error())
		return
	}

	csvData := make([][]string, 0)
	csvData = append(csvData, (&domain.User{}).GetHeaders())

	buf := new(bytes.Buffer)
	wr := csv.NewWriter(buf)

	for _, user := range users {
		csvData = append(csvData, user.ToSlice())
	}

	wr.WriteAll(csvData)
	wr.Flush()

	encryptedFileName := fmt.Sprintf("encrypted-users-%s.csv", date.Format(time.Now()))
	err = encryptUpload(buf, encryptedFileName)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Generate Users CSV Done!")
}

func generateTransactionsCsv() {
	log.Println("Generating Transactions CSV...")

	var startTimestampInt int64 = -1
	var endTimestampInt int64 = -1
	transactions, err := transactionSvc.GetTransactions(startTimestampInt, endTimestampInt)
	if err != nil {
		log.Println(err.Error())
		return
	}

	csvData := make([][]string, 0)
	csvData = append(csvData, (&domain.Transaction{}).GetHeaders())
	for _, transaction := range transactions {
		csvData = append(csvData, transaction.ToSlice())
	}

	buf := new(bytes.Buffer)
	wr := csv.NewWriter(buf)
	wr.WriteAll(csvData)
	wr.Flush()

	encryptedFileName := fmt.Sprintf("encrypted-transactions-%s.csv", date.Format(time.Now()))
	err = encryptUpload(buf, encryptedFileName)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Generate Transactions CSV Done!")
}

func encryptUpload(buf *bytes.Buffer, fileName string) error {
	err := encrypt.Encrypt(buf, fileName)
	if err != nil {
		return errors.Errorf("encryptUpload Error -> %s", err)
	}

	err = sftp.Upload(fileName)
	if err != nil {
		return errors.Errorf("encryptUpload Error -> %s", err)
	}

	err = os.Remove("downloads/" + fileName)
	if err != nil {
		return errors.Errorf("encryptUpload Error -> %s", err)
	}

	return nil
}

func getUserCollections() (domain.UserCollections, error) {
	userAddrs, err := web3Util.GetAllAddrs()
	if err != nil {
		return nil, errors.Errorf("web3 GetAllAddrs Error -> %s", err)
	}

	var users domain.UserCollections

	for _, userAddr := range userAddrs {
		var userData []byte
		userData, err = web3Util.GetUser(web3.BlockchainAddress(userAddr.String()))
		if err != nil {
			return nil, errors.Errorf("getUserCollections Error -> %s", err)
		}

		var userCollection domain.UserCollection

		err = json.Unmarshal(userData, &userCollection)
		if err != nil {
			return nil, errors.Errorf("getUserCollections Error -> %s", err)
		}

		users = append(users, &userCollection)
	}

	return users, nil
}
