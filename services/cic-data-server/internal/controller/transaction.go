package controller

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"

	"github.com/gin-gonic/gin"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/service"
)

type TransactionController interface {
	GetTransactions(c *gin.Context)
	GetSpendersReport(c *gin.Context)
}

type transactionController struct {
	svc *service.TransactionService
}

func NewTransaction(svc *service.TransactionService) TransactionController {
	return &transactionController{
		svc: svc,
	}
}

func (controller *transactionController) GetTransactions(c *gin.Context) {
	startTimestamp := c.Query("start_timestamp")
	endTimestamp := c.Query("end_timestamp")

	var startTimestampInt int64 = -1
	var endTimestampInt int64 = -1
	var err error = nil

	if startTimestamp != "" {
		startTimestampInt, err = strconv.ParseInt(startTimestamp, 10, 64)
		if err != nil || !date.ValidateTimestamp(startTimestampInt) {
			c.JSON(http.StatusBadRequest, gin.H{"message": "start_timestamp is not valid"})
			return
		}
	}

	if endTimestamp != "" {
		endTimestampInt, err = strconv.ParseInt(endTimestamp, 10, 64)
		if err != nil || !date.ValidateTimestamp(endTimestampInt) {
			c.JSON(http.StatusBadRequest, gin.H{"message": "end_timestamp is not valid"})
			return
		}
	}

	if startTimestamp != "" && endTimestamp != "" {
		if startTimestampInt > endTimestampInt {
			c.JSON(http.StatusBadRequest, gin.H{"message": "end_timestamp cannot be earlier than start_timestamp."})
			return
		}
	}

	transactions, err := controller.svc.GetTransactions(startTimestampInt, endTimestampInt)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Internal Server Error"})
		return
	}

	csvData := make([][]string, 0)
	csvData = append(csvData, (&domain.Transaction{}).GetHeaders())

	dataBytes := new(bytes.Buffer)
	wr := csv.NewWriter(dataBytes)

	for _, transaction := range transactions {
		csvData = append(csvData, transaction.ToSlice())
	}

	wr.WriteAll(csvData)
	wr.Flush()

	c.Writer.Header().Set("Content-Type", "text/csv")
	c.Writer.Header().Set("Content-Disposition", fmt.Sprintf("attachment;filename=%s", "transactions.csv"))
	c.String(http.StatusOK, dataBytes.String())
}

func (controller *transactionController) GetSpendersReport(c *gin.Context) {
	startTimestamp := c.Query("start_timestamp")
	endTimestamp := c.Query("end_timestamp")

	var startTimestampInt int64 = -1
	var endTimestampInt int64 = -1
	var err error = nil

	if startTimestamp != "" && endTimestamp != "" {
		startTimestampInt, err = strconv.ParseInt(startTimestamp, 10, 64)
		if err != nil || !date.ValidateTimestamp(startTimestampInt) {
			c.JSON(http.StatusBadRequest, gin.H{"message": "start_timestamp is not valid"})
			return
		}

		endTimestampInt, err = strconv.ParseInt(endTimestamp, 10, 64)
		if err != nil || !date.ValidateTimestamp(endTimestampInt) {
			c.JSON(http.StatusBadRequest, gin.H{"message": "end_timestamp is not valid"})
			return
		}

		if startTimestampInt > endTimestampInt {
			c.JSON(http.StatusBadRequest, gin.H{"message": "end_timestamp cannot be earlier than start_timestamp."})
			return
		}
	} else {
		// if startTimestamp or endTimestamp is not provided, set as default datetime
		// default is past 7 days
		startTimestampInt, endTimestampInt = date.LastWeek()
	}

	transactions, err := controller.svc.GetSpendersReport(startTimestampInt, endTimestampInt)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Internal Server Error"})
		return
	}

	log.Println(len(transactions))

	csvData := make([][]string, 0)
	csvData = append(csvData, (&domain.TransactionReport{}).GetHeaders())

	dataBytes := new(bytes.Buffer)
	wr := csv.NewWriter(dataBytes)

	for _, transaction := range transactions {
		csvData = append(csvData, transaction.ToSlice())
	}

	wr.WriteAll(csvData)
	wr.Flush()

	c.Writer.Header().Set("Content-Type", "text/csv")
	c.Writer.Header().Set("Content-Disposition", fmt.Sprintf("attachment;filename=%s", "transactions-spenderReport.csv"))
	c.String(http.StatusOK, dataBytes.String())
}