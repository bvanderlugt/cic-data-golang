package domain

import (
	"strconv"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
)

type Transaction struct {
	ID                      int
	TransactionHash         string
	BlockNumber             int
	BlockTime               time.Time
	SenderAddress           string
	ReceiverAddress         string
	AmountSent              *bigint.Bigint
	AmountReceived          *bigint.Bigint
	TransactionType         string
	SenderToken             string
	ReceiverToken           string
	SenderRegistered        time.Time
	SenderProductCategory   string
	SenderGender            string
	SenderAge               int
	SenderAreaName          string
	SenderAreaType          string
	ReceiverRegistered      time.Time
	ReceiverProductCategory string
	ReceiverGender          string
	ReceiverAge             int
	ReceiverAreaName        string
	ReceiverAreaType        string
}

func (t *Transaction) GetHeaders() []string {
	return []string{
		"ID",
		"TransactionHash",
		"BlockNumber",
		"BlockTime",
		"SenderAddress",
		"ReceiverAddress",
		"AmountSent",
		"AmountReceived",
		"TransactionType",
		"SenderToken",
		"ReceiverToken",
		"SenderRegistered",
		"SenderProductCategory",
		"SenderGender",
		"SenderAge",
		"SenderAreaName",
		"SenderAreaType",
		"ReceiverRegistered",
		"ReceiverProductCategory",
		"ReceiverGender",
		"ReceiverAge",
		"ReceiverAreaName",
		"ReceiverAreaType",
	}
}

func (t *Transaction) ToSlice() []string {
	return []string{
		strconv.Itoa(t.ID),
		t.TransactionHash,
		strconv.Itoa(t.BlockNumber),
		date.Format(t.BlockTime),
		t.SenderAddress,
		t.ReceiverAddress,
		t.AmountSent.String(),
		t.AmountReceived.String(),
		t.TransactionType,
		t.SenderToken,
		t.ReceiverToken,
		date.Format(t.SenderRegistered),
		t.SenderProductCategory,
		t.SenderGender,
		strconv.Itoa(t.SenderAge),
		t.SenderAreaName,
		t.SenderAreaType,
		date.Format(t.ReceiverRegistered),
		t.ReceiverProductCategory,
		t.ReceiverGender,
		strconv.Itoa(t.ReceiverAge),
		t.ReceiverAreaName,
		t.ReceiverAreaType,
	}
}

type Transactions []*Transaction

type TransactionReport struct {
	SenderAddress           string
	Count	int
}

func (t *TransactionReport) GetHeaders() []string {
	return []string{
		"SenderAddress",
		"StandardTransactionNum",
	}
}

func (t *TransactionReport) ToSlice() []string {
	return []string{
		t.SenderAddress,
		strconv.Itoa(t.Count),
	}
}

type TransactionReports []*TransactionReport