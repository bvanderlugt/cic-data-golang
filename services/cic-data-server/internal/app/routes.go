package app

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/controller"
)

func routes(router *gin.Engine, user controller.UserController, transaction controller.TransactionController) {
	router.GET("/health", func(c *gin.Context) {
		c.Status(http.StatusOK)
	})

	router.GET("/users", user.GetAllUser)
	router.GET("/users/:blockchain_address", user.GetUser)
	router.POST("/users/:blockchain_address", user.UpdatePendingUser)

	router.GET("/transactions", transaction.GetTransactions)
	router.GET("/transactions/getSpenderReport", transaction.GetSpendersReport)
}
