package app

import (
	"fmt"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/httputil"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/server"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/global"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/controller"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/service"
)

func init() {
	global.Init()
}

var (
	web3Util       *web3.Web3
)

func StartApplication() {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()

	logger, err := server.NewZapLogger()
	if err != nil {
		log.Fatalf("cannot create logger: %v \n", err)
	}

	web3Util, err = web3.NewWeb3(httputil.NewDefaultClient(), &web3.Conf{
		config.Conf.Web3Provider.Host,
		config.Conf.Web3Provider.Port,
		config.Conf.DataProvider.UserHost,
		config.Conf.DataProvider.UserPort,
		config.Conf.Web3Provider.AccountRegistryAddress,
		config.Conf.Web3Provider.TokenContractAddress,
	})
	if err != nil {
		log.Fatalf("cannot connect to web3: %v", err)
	}

	userController := controller.NewUser(&service.UserService{
		PG:     &dao.UserPG{},
		Web3:   web3Util,
		Logger: logger,
	})
	transactionController := controller.NewTransaction(&service.TransactionService{
		PG:     &dao.TransactionPG{},
		Logger: logger,
	})

	routes(router, userController, transactionController)

	server := getServer(router)

	closed := make(chan struct{})
	go waitForShutdown(server, closed)

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Panic("Error when trying to start the server", err)
	}

	<-closed
}

func getServer(router *gin.Engine) *http.Server {
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%s", config.Conf.Server.Port),
		Handler:      router,
		ReadTimeout:  config.Conf.Server.TimeoutRead,
		WriteTimeout: config.Conf.Server.TimeoutWrite,
		IdleTimeout:  config.Conf.Server.TimeoutIdle,
	}
	return srv
}
