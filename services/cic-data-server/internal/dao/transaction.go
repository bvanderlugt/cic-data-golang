package dao

import (
	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/domain"
)

type TransactionPG struct{}

func (t *TransactionPG) GetTransactions(startTimestampInt int64, endTimestampInt int64) ([]*domain.Transaction, error) {
	db := pg.Client()

	var transactions domain.Transactions

	if startTimestampInt != -1 && endTimestampInt != -1 {
		startDateTime := date.ParseUnixTimestamp(startTimestampInt)
		endDateTime := date.ParseUnixTimestamp(endTimestampInt)
		if err := db.Raw("SELECT * FROM transactions WHERE block_time >= ? AND block_time <= ? ORDER BY block_time, id", startDateTime, endDateTime).Scan(&transactions).Error; err != nil {
			return nil, errors.Errorf("Repository/Transaction -> GetTransactions Error -> %s", err)
		}
	} else if startTimestampInt != -1 && endTimestampInt == -1 {
		startDateTime := date.ParseUnixTimestamp(startTimestampInt)
		if err := db.Raw("SELECT * FROM transactions WHERE block_time >= ? ORDER BY block_time, id", startDateTime).Scan(&transactions).Error; err != nil {
			return nil, errors.Errorf("Repository/Transaction -> GetTransactions Error -> %s", err)
		}
	} else if startTimestampInt == -1 && endTimestampInt != -1 {
		endDateTime := date.ParseUnixTimestamp(endTimestampInt)
		if err := db.Raw("SELECT * FROM transactions WHERE block_time <= ? ORDER BY block_time, id", endDateTime).Scan(&transactions).Error; err != nil {
			return nil, errors.Errorf("Repository/Transaction -> GetTransactions Error -> %s", err)
		}
	} else {
		if err := db.Raw("SELECT * FROM transactions ORDER BY id").Scan(&transactions).Error; err != nil {
			return nil, errors.Errorf("Repository/Transaction -> GetTransactions Error -> %s", err)
		}
	}

	return transactions, nil
}

func (t *TransactionPG) GetSpendersReport(startTimestampInt int64, endTimestampInt int64) ([]*domain.TransactionReport, error) {
	db := pg.Client()

	var transactions domain.TransactionReports

	startDateTime := date.ParseUnixTimestamp(startTimestampInt)
	endDateTime := date.ParseUnixTimestamp(endTimestampInt)

	if err := db.Raw("SELECT sender_address, COUNT(*) FROM transactions WHERE transaction_type = 'standard' AND block_time between ? AND ? GROUP BY sender_address HAVING COUNT(*) > 0", startDateTime, endDateTime).Scan(&transactions).Error; err != nil {
		return nil, errors.Errorf("Repository/Transaction -> GetSpendersReport Error -> %s", err)
	}

	return transactions, nil
}
