package main

import "gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-server/internal/app"

func main() {
	app.StartApplication()
}
