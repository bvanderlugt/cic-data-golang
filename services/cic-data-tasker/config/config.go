package config

import (
	"log"
	"time"

	"github.com/caarlos0/env/v6"
)

var Conf = config{}

type config struct {
	Server        serverConf
	PG            pgConf
	UserResource  userResourceConf
	SystemAddress systemAddressConf
	BlockNumber   blockNumberConf
	DataProvider  dataProviderConf
	Web3Provider  web3Conf
}

type serverConf struct {
	Port         string        `env:"SERVER_PORT,required"`
	TimeoutRead  time.Duration `env:"SERVER_TIMEOUT_READ,required"`
	TimeoutWrite time.Duration `env:"SERVER_TIMEOUT_WRITE,required"`
	TimeoutIdle  time.Duration `env:"SERVER_TIMEOUT_IDLE,required"`
}

type pgConf struct {
	USER     string `env:"PG_USER,required"`
	PASSWORD string `env:"PG_PASSWORD,required"`
	HOST     string `env:"PG_HOST,required"`
	DBName   string `env:"PG_DB_NAME,required"`
	PORT     string `env:"PG_PORT,required"`
}

type userResourceConf struct {
	AreaNameHost        string `env:"USER_AREA_NAME_HOST,required"`
	AreaTypeHost        string `env:"USER_AREA_TYPE_HOST,required"`
	ProductCategoryHost string `env:"USER_PRODUCT_CATEGORY_HOST,required"`
}

type systemAddressConf struct {
	TrustAddress  string `env:"TRUST_ADDRESS,required"`
	FaucetAddress string `env:"FAUCET_ADDRESS,required"`
}

type blockNumberConf struct {
	BootstrappingEnd          int `env:"BOOTSTRAPPING_END_BLOCK_NUMBER:"`
	UpdateTransactionDecrease int `env:"UPDATE_TRANSACTION_BLOCK_DECREASE_NUMBER"`
	UpdateTransactionIncrease int `env:"UPDATE_TRANSACTION_BLOCK_INCREASE_NUMBER"`
}

type dataProviderConf struct {
	TransactionHost string `env:"DATA_PROVIDER_TRANSACTIONS_HOST"`
	TransactionPort string `env:"DATA_PROVIDER_TRANSACTIONS_PORT"`
	UserHost        string `env:"DATA_PROVIDER_USERS_HOST"`
	UserPort        string `env:"DATA_PROVIDER_USERS_PORT"`
}

type web3Conf struct {
	Host                   string `env:"WEB3_HOST"`
	Port                   string `env:"WEB3_PORT"`
	AccountRegistryAddress string `env:"WEB3_ACCOUNT_REGISTRY_ADDRESS"`
	TokenContractAddress   string `env:"WEB3_TOKEN_CONTRACT_ADDRESS"`
}

func Init() {
	err := env.Parse(&Conf)
	if err != nil {
		log.Fatalf("Failed to decode environment variables: %s", err)
	}
}
