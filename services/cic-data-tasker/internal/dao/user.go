package dao

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gorm.io/gorm"
)

type UserPG struct{}

func (u *UserPG) AddUser(db *gorm.DB, user *domain.UserEntity) error {
	if err := db.Exec(
		"INSERT INTO users("+
			"blockchain_address,"+
			"date_registered,"+
			"days_enrolled,"+
			"product_category,"+
			"gender,"+
			"age,"+
			"area_name,"+
			"area_type,"+
			"account_type,"+
			"preferred_language,"+
			"is_market_enabled,"+
			"updated_at"+
			") VALUES ("+
			"@BlockchainAddress,"+
			"@DateRegistered,"+
			"@DaysEnrolled,"+
			"@ProductCategory,"+
			"@Gender,"+
			"@Age,"+
			"@AreaName,"+
			"@AreaType,"+
			"@AccountType,"+
			"@PreferredLanguage,"+
			"@IsMarketEnabled,"+
			"@UpdatedAt)",
		user).Error; err != nil {
		return errors.Errorf("Repository/User -> AddUser Error -> %s", err)
	}
	return nil
}

func (u *UserPG) FindUserByBlockchainAddress(db *gorm.DB, blockchainAddress string) (*domain.UserEntity, error) {
	var user domain.UserEntity
	trustAddress := config.Conf.SystemAddress.TrustAddress
	faucetAddress := config.Conf.SystemAddress.FaucetAddress

	if blockchainAddress == trustAddress || blockchainAddress == faucetAddress {
		user = domain.UserEntity{
			BlockchainAddress: "SYSTEM_ADDRESS",
			DateRegistered:    time.Date(1970, time.Month(1), 1, 0, 0, 0, 0, time.UTC),
			Age:               0,
			ProductCategory:   "system",
			AreaType:          "system",
			AreaName:          "system",
			Gender:            "system",
		}
	} else {
		if err := db.Raw("SELECT * FROM users WHERE blockchain_address = ? ORDER BY id", blockchainAddress).Scan(&user).Error; err != nil {
			return nil, errors.Errorf("Repository/User -> FindUserByBlockchainAddress Error -> %s", err)
		}
	}

	return &user, nil
}

func (u *UserPG) UpdateUser(db *gorm.DB, user domain.UserEntity) error {
	if err := db.Exec(
		"UPDATE users SET "+
			"date_registered = @DateRegistered,"+
			"days_enrolled = @DaysEnrolled,"+
			"product_category = @ProductCategory,"+
			"gender = @Gender,"+
			"age = @Age,"+
			"area_name = @AreaName,"+
			"area_type = @AreaType,"+
			"updated_at = @UpdatedAt,"+
			"account_type = @AccountType,"+
			"preferred_language = @PreferredLanguage,"+
			"is_market_enabled = @IsMarketEnabled,"+
			"pending_update = @PendingUpdate,"+
			"token_balance = @TokenBalance,"+
			"ovol_in = @OvolIn,"+
			"ovol_out = @OvolOut,"+
			"otxns_in = @OtxnsIn,"+
			"otxns_out = @OtxnsOut,"+
			"ounique_in = @OuniqueIn,"+
			"ounique_out = @OuniqueOut,"+
			"svol_in = @SvolIn,"+
			"svol_out = @SvolOut,"+
			"stxns_in = @StxnsIn,"+
			"stxns_out = @StxnsOut,"+
			"sunique_in = @SuniqueIn,"+
			"sunique_out = @SuniqueOut,"+
			"sunique_all = @SuniqueAll,"+
			"last_trade_out = @LastTradeOut,"+
			"first_trade_in_user = @FirstTradeInUser,"+
			"first_trade_in_time = @FirstTradeInTime "+
			"WHERE blockchain_address = @BlockchainAddress",
		user).Error; err != nil {
		return errors.Errorf("Repository/User -> UpdateUser Error -> %s", err)
	}
	return nil
}

func (u *UserPG) GetAllUser(db *gorm.DB) ([]*domain.UserEntity, error) {
	var users domain.Users
	if err := db.Raw("SELECT * FROM users ORDER BY id").Scan(&users).Error; err != nil {
		return nil, errors.Errorf("Repository/User -> GetAllUser Error -> %s", err)
	}

	return users, nil
}

func (u *UserPG) FindPendingUser(db *gorm.DB) ([]*domain.UserEntity, error) {
	var users domain.Users
	if err := db.Raw("SELECT * FROM users WHERE pending_update = true ORDER BY id").Scan(&users).Error; err != nil {
		return nil, errors.Errorf("Repository/User -> FindPendingUser Error -> %s", err)
	}

	return users, nil
}

func (u *UserPG) UpdateUserTokenBalance(db *gorm.DB, user *domain.UserEntity) error {
	if err := db.Exec(
		"UPDATE users SET token_balance = @TokenBalance, updated_at = @UpdatedAt WHERE blockchain_address = @BlockchainAddress", user).Error; err != nil {
		return errors.Errorf("Repository/User -> UpdateUserTokenBalance Error -> %s", err)
	}
	return nil
}
