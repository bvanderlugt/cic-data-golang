package dao

import (
	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gorm.io/gorm"
)

type TransactionPG struct{}

func (t *TransactionPG) AddTransaction(db *gorm.DB, transaction domain.TransactionEntity) error {
	if err := db.Exec(
		"INSERT INTO transactions("+
			"transaction_hash,"+
			"block_number,"+
			"block_time,"+
			"sender_address,"+
			"receiver_address,"+
			"amount_sent,"+
			"amount_received,"+
			"transaction_type,"+
			"sender_token,"+
			"receiver_token,"+
			"sender_registered,"+
			"sender_product_category,"+
			"sender_gender,"+
			"sender_age,"+
			"sender_area_name,"+
			"sender_area_type,"+
			"receiver_registered,"+
			"receiver_product_category,"+
			"receiver_gender,"+
			"receiver_age,"+
			"receiver_area_name,"+
			"receiver_area_type"+
			") VALUES ("+
			"@TransactionHash,"+
			"@BlockNumber,"+
			"@BlockTime,"+
			"@SenderAddress,"+
			"@ReceiverAddress,"+
			"@AmountSent,"+
			"@AmountReceived,"+
			"@TransactionType,"+
			"@SenderToken,"+
			"@ReceiverToken,"+
			"@SenderRegistered,"+
			"@SenderProductCategory,"+
			"@SenderGender,"+
			"@SenderAge,"+
			"@SenderAreaName,"+
			"@SenderAreaType,"+
			"@ReceiverRegistered,"+
			"@ReceiverProductCategory,"+
			"@ReceiverGender,"+
			"@ReceiverAge,"+
			"@ReceiverAreaName,"+
			"@ReceiverAreaType)",
		transaction).Error; err != nil {
		return errors.Errorf("Repository/Transaction -> AddTransaction Error -> %s", err)
	}
	return nil
}

func (t *TransactionPG) FindTransaction(db *gorm.DB, txHash string) (*domain.TransactionEntity, error) {
	var transaction domain.TransactionEntity
	if err := db.Raw("SELECT * FROM transactions WHERE transaction_hash = ?", txHash).Scan(&transaction).Error; err != nil {
		return nil, errors.Errorf("Repository/Transaction -> FindTransaction Error -> %s", err)
	}

	return &transaction, nil
}

func (t *TransactionPG) IsTransactionUnique(db *gorm.DB, senderAddress string, receiverAddress string) (bool, error) {
	var result int
	if err := db.Raw("SELECT count(sender_address) FROM transactions WHERE sender_address = ? AND receiver_address = ?", senderAddress, receiverAddress).Scan(&result).Error; err != nil {
		return false, errors.Errorf("Repository/Transaction -> IsTransactionUnique Error -> %s", err)
	}

	if result == 0 {
		return true, nil
	}
	return false, nil
}

func (t *TransactionPG) SelectHighestBlock(db *gorm.DB) (int, error) {
	var result int
	if err := db.Raw("SELECT coalesce(MAX(block_number), 0) as Max FROM transactions").Scan(&result).Error; err != nil {
		return 0, errors.Errorf("Repository/Transaction -> SelectHighestBlock Error -> %s", err)
	}

	return result, nil
}
