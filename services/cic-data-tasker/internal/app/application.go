package app

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/global"
)

func init() {
	global.Init()
}

func StartApplication() {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()

	routes(router)

	server := getServer(router)

	closed := make(chan struct{})
	go waitForShutdown(server, closed)

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Panic("Error when trying to start the server", err)
	}

	<-closed
}

func getServer(router *gin.Engine) *http.Server {
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%s", config.Conf.Server.Port),
		Handler:      router,
		ReadTimeout:  config.Conf.Server.TimeoutRead,
		WriteTimeout: config.Conf.Server.TimeoutWrite,
		IdleTimeout:  config.Conf.Server.TimeoutIdle,
	}
	return srv
}
