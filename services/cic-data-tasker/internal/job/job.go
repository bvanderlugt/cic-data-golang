package job

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/httputil"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/server"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"

	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/service"

	"github.com/go-co-op/gocron"
)

var (
	userSvc        *service.UserService
	transactionSvc *service.TransactionService
	s              *gocron.Scheduler
	web3Util       *web3.Web3
)

func Init() {
	var err error
	web3Util, err = web3.NewWeb3(httputil.NewDefaultClient(), &web3.Conf{
		config.Conf.Web3Provider.Host,
		config.Conf.Web3Provider.Port,
		config.Conf.DataProvider.UserHost,
		config.Conf.DataProvider.UserPort,
		config.Conf.Web3Provider.AccountRegistryAddress,
		config.Conf.Web3Provider.TokenContractAddress,
	})
	if err != nil {
		log.Fatalf("cannot connect to web3: %v", err)
	}
	logger, err := server.NewZapLogger()
	if err != nil {
		log.Fatalf("cannot create logger: %v \n", err)
	}

	userSvc = &service.UserService{
		PG:     &dao.UserPG{},
		Web3:   web3Util,
		Logger: logger,
	}
	transactionSvc = &service.TransactionService{
		UserPG:        &dao.UserPG{},
		TransactionPG: &dao.TransactionPG{},
		Web3:          web3Util,
		Logger:        logger,
	}

	bootstrapping()
	activateJobs()
}

func activateJobs() {
	s = gocron.NewScheduler(time.UTC)
	s.Every(1).Minute().SingletonMode().Do(updateTransactions)
	s.Every(1).Minute().SingletonMode().Do(updateUsers)
	s.StartAsync()
}

func bootstrapping() {
	log.Println("Bootstrapping...")

	userCollections, err := getUserCollections()
	if err != nil {
		log.Println(errors.Errorf("Job -> bootstrapping Error -> %s", err))
		return
	}

	// pass data to service
	err = userSvc.ProcessAllUser(userCollections)
	if err != nil {
		log.Println(errors.Errorf("Job -> bootstrapping Error -> %s", err))
		return
	}

	startBlock := 0
	endBlock := config.Conf.BlockNumber.UpdateTransactionIncrease

	log.Printf("get transaction from %v to %v\n", startBlock, endBlock)

	transactionCollections, err := getTransactionCollections(startBlock, endBlock)
	if err != nil {
		log.Println(errors.Errorf("Job -> bootstrapping Error -> %s", err))
		return
	}

	// pass data to service
	err = transactionSvc.ProcessAllTransaction(transactionCollections)
	if err != nil {
		log.Println(errors.Errorf("Job -> bootstrapping Error -> %s", err))
		return
	}

	log.Println("Bootstrapping Done!")
}

func getUserCollections() (domain.UserCollections, error) {
	userAddrs, err := web3Util.GetAllAddrs()
	if err != nil {
		return nil, errors.Errorf("web3 GetAllAddrs Error -> %s", err)
	}

	var users domain.UserCollections

	for _, userAddr := range userAddrs {
		var userData []byte
		userData, err = web3Util.GetUser(web3.BlockchainAddress(userAddr.String()))
		if err != nil {
			return nil, errors.Errorf("getUserCollections Error -> %s", err)
		}

		var userCollection domain.UserCollection

		err = json.Unmarshal(userData, &userCollection)
		if err != nil {
			return nil, errors.Errorf("getUserCollections Error -> %s", err)
		}

		users = append(users, &userCollection)
	}

	return users, nil
}

func updateTransactions() {
	log.Println("Updating Transactions...")

	err := userSvc.UpdatePendingUser()
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	latestBlockNum, err := transactionSvc.SelectHighestBlock()
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	updateTransactionDecrease := config.Conf.BlockNumber.UpdateTransactionDecrease

	startBlock := latestBlockNum

	if latestBlockNum > updateTransactionDecrease {
		startBlock = latestBlockNum - config.Conf.BlockNumber.UpdateTransactionDecrease
	}

	endBlock := latestBlockNum + config.Conf.BlockNumber.UpdateTransactionIncrease

	log.Printf("get transaction from %v to %v\n", startBlock, endBlock)

	transactionCollections, err := getTransactionCollections(startBlock, endBlock)
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	// pass data to service
	err = transactionSvc.ProcessAllTransaction(transactionCollections)
	if err != nil {
		log.Println(errors.Errorf("Job -> updateTransactions Error -> %s", err))
		return
	}

	log.Println("Update Transactions Done!")
}

func getTransactionCollections(startBlock int, endBlock int) (domain.TransactionCollections, error) {
	url := fmt.Sprintf("http://%s:%s/txa/%d/%d", config.Conf.DataProvider.TransactionHost, config.Conf.DataProvider.TransactionPort, startBlock, endBlock)

	client := http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}
	req.Header.Set("X-CIC-CACHE-MODE", "all")
	res, err := client.Do(req)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}

	if res.StatusCode != 200 {
		return nil, errors.Errorf("getUserCollections Error -> Request Transactions Data Error")
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}

	var transactionGetData domain.TransactionGetData

	err = json.Unmarshal(body, &transactionGetData)
	if err != nil {
		return nil, errors.Errorf("getTransactionCollections Error -> %s", err)
	}

	return transactionGetData.TransactionCollections, nil
}

func updateUsers() {
	log.Println("Updating Users...")

	err := userSvc.UpdateUserTokenBalance()
	if err != nil {
		log.Println(errors.Errorf("Job -> UpdateUserTokenBalance Error -> %s", err))
		return
	}

	log.Println("Update Users Done!")
}
