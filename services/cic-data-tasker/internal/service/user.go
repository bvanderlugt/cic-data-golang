package service

import (
	"encoding/json"
	"log"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/dao"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/domain"
)

type UserService struct {
	PG     *dao.UserPG
	Web3   *web3.Web3
	Logger *zap.Logger
}

func (s *UserService) ProcessAllUser(userCollections domain.UserCollections) error {
	db := pg.Client()

	for _, userCollection := range userCollections {
		user, err := userCollection.ToUser()
		if err != nil {
			s.Logger.Error("cannot convert user", zap.Error(err))
			continue
		}

		// check user existed or not
		searchUserData, err := s.PG.FindUserByBlockchainAddress(db, user.BlockchainAddress)
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		if searchUserData.BlockchainAddress != "" {
			continue
		}

		// getUserType and Preferences
		var accountType string
		accountType, err = s.Web3.GetAccountType(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		user.AccountType = accountType

		var (
			preferredLanguage string
			isMarketEnabled   bool
		)
		preferredLanguage, isMarketEnabled, err = s.Web3.GetPreferences(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		user.PreferredLanguage = preferredLanguage
		user.IsMarketEnabled = isMarketEnabled

		tx := db.Begin()
		err = s.PG.AddUser(tx, user)
		if err != nil {
			tx.Rollback()
			return errors.Errorf("Service/User -> ProcessAllUser Error -> %s", err)
		}
		tx.Commit()
	}

	return nil
}

func (s *UserService) UpdatePendingUser() error {
	db := pg.Client()

	users, err := s.PG.FindPendingUser(db)
	if err != nil {
		return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
	}

	for _, user := range users {
		var web3UserData []byte
		web3UserData, err = s.Web3.GetUser(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			// return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
			log.Printf("Service/User -> Web3 GetUser Error")
			continue
		}

		var userCollection domain.UserCollection

		err = json.Unmarshal(web3UserData, &userCollection)
		if err != nil {
			return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
		}

		var userData *domain.UserEntity
		userData, err = userCollection.ToUser()
		if err != nil {
			s.Logger.Error("cannot convert user", zap.Error(err))
			continue
		}

		// choose the data we want to update
		user.DateRegistered = userData.DateRegistered
		user.DaysEnrolled = userData.DaysEnrolled
		user.ProductCategory = userData.ProductCategory
		user.Gender = userData.Gender
		user.Age = userData.Age
		user.AreaName = userData.AreaName
		user.AreaType = userData.AreaType
		user.PendingUpdate = false
		user.UpdatedAt = userData.UpdatedAt

		err = s.PG.UpdateUser(db, *user)
		if err != nil {
			return errors.Errorf("Service/User -> UpdatePendingUser Error -> %s", err)
		}
	}

	return nil
}

func (s *UserService) UpdateUserTokenBalance() error {
	db := pg.Client()

	users, err := s.PG.GetAllUser(db)
	if err != nil {
		return errors.Errorf("Service/User -> UpdateUserTokenBalance Error -> %s", err)
	}

	for _, user := range users {
		var tokenBalance *bigint.Bigint
		tokenBalance, err := s.Web3.GetUserTokenBalance(web3.BlockchainAddress(user.BlockchainAddress))
		if err != nil {
			return errors.Errorf("Service/User -> UpdateUserTokenBalance Error -> %s", err)
		}
		user.TokenBalance = tokenBalance
		user.UpdatedAt = time.Now()
		err = s.PG.UpdateUserTokenBalance(db, user)
		if err != nil {
			return errors.Errorf("Service/User -> UpdateUserTokenBalance Error -> %s", err)
		}
	}

	return nil
}
