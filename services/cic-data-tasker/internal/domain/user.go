package domain

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/util"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
)

type UserEntity struct {
	ID                int
	UpdatedAt         time.Time
	PendingUpdate     bool
	BlockchainAddress string
	DateRegistered    time.Time
	DaysEnrolled      int
	ProductCategory   string
	Gender            string
	Age               int
	AreaName          string
	AreaType          string
	AccountType       string
	PreferredLanguage string
	IsMarketEnabled   bool
	TokenBalance      *bigint.Bigint
	OvolIn            *bigint.Bigint
	OvolOut           *bigint.Bigint
	OtxnsIn           int
	OtxnsOut          int
	OuniqueIn         int
	OuniqueOut        int
	SvolIn            *bigint.Bigint
	SvolOut           *bigint.Bigint
	StxnsIn           int
	StxnsOut          int
	SuniqueIn         int
	SuniqueOut        int
	SuniqueAll        int
	LastTradeOut      time.Time
	FirstTradeInUser  string
	FirstTradeInTime  time.Time
}

func (user *UserEntity) GetHeaders() []string {
	return []string{
		"ID",
		"UpdatedAt",
		"PendingUpdate",
		"BlockchainAddress",
		"DateRegistered",
		"DaysEnrolled",
		"ProductCategory",
		"Gender",
		"Age",
		"AreaName",
		"AreaType",
		"AccountType",
		"PreferredLanguage",
		"IsMarketEnabled",
		"TokenBalance",
		"OvolIn",
		"OvolOut",
		"OtxnsIn",
		"OtxnsOut",
		"OuniqueIn",
		"OuniqueOut",
		"SvolIn",
		"SvolOut",
		"StxnsIn",
		"StxnsOut",
		"SuniqueIn",
		"SuniqueOut",
		"SuniqueAll",
		"LastTradeOut",
		"FirstTradeInUser",
		"FirstTradeInTime",
	}
}

func (user *UserEntity) ToSlice() []string {
	return []string{
		strconv.Itoa(user.ID),
		date.Format(user.UpdatedAt),
		strconv.FormatBool(user.PendingUpdate),
		user.BlockchainAddress,
		date.Format(user.DateRegistered),
		strconv.Itoa(user.DaysEnrolled),
		user.ProductCategory,
		user.Gender,
		strconv.Itoa(user.Age),
		user.AreaName,
		user.AreaType,
		user.AccountType,
		user.PreferredLanguage,
		strconv.FormatBool(user.IsMarketEnabled),
		user.TokenBalance.String(),
		user.OvolIn.String(),
		user.OvolOut.String(),
		strconv.Itoa(user.OtxnsIn),
		strconv.Itoa(user.OtxnsOut),
		strconv.Itoa(user.OuniqueIn),
		strconv.Itoa(user.OuniqueOut),
		user.SvolIn.String(),
		user.SvolOut.String(),
		strconv.Itoa(user.StxnsIn),
		strconv.Itoa(user.StxnsOut),
		strconv.Itoa(user.SuniqueIn),
		strconv.Itoa(user.SuniqueOut),
		strconv.Itoa(user.SuniqueAll),
		date.Format(user.LastTradeOut),
		user.FirstTradeInUser,
		date.Format(user.FirstTradeInTime),
	}
}

type Users []*UserEntity

type UserCollections []*UserCollection

type UserCollection struct {
	DateRegistered int         `json:"date_registered"`
	Gender         string      `json:"gender"`
	Identity       Identities  `json:"identities"`
	Location       Location    `json:"location"`
	Products       []string    `json:"products"`
	DateOfBirth    DateOfBirth `json:"date_of_birth"`
}

type Identities struct {
	Evm Evm `json:"evm"`
}

type Evm struct {
	Bloxberg []string `json:"bloxberg:8996"`
}

type Location struct {
	AreaName string `json:"area_name"`
}

type DateOfBirth struct {
	Year  int `json:"year"`
	Month int `json:"month"`
	Day   int `json:"day"`
}

type UserPreferences struct {
	PreferredLanguage string `json:"preferred_language"`
	IsMarketEnabled   bool   `json:"is_market_enabled"`
}

func (userCollection *UserCollection) ToUser() (*UserEntity, error) {
	if len(userCollection.Identity.Evm.Bloxberg) == 0 {
		return nil, errors.New("empty blockchain address")
	}

	// data calculation
	blockchainAddress := userCollection.Identity.Evm.Bloxberg[0]
	dateRegistered := date.ParseUnixTimestamp(int64(userCollection.DateRegistered))
	daysEnrolled := calculateDaysEnrolled(userCollection.DateRegistered)
	productCategory := getKeyByValue(userCollection.Products[0], "product_categories")
	gender := userCollection.Gender
	age := calculateAge(userCollection.DateOfBirth)
	areaName := getKeyByValue(userCollection.Location.AreaName, "area_names")
	areaType := getKeyByValue(areaName, "area_types")
	updatedAt := time.Now()

	return &UserEntity{
		BlockchainAddress: blockchainAddress,
		DateRegistered:    dateRegistered,
		DaysEnrolled:      daysEnrolled,
		ProductCategory:   productCategory,
		Gender:            gender,
		Age:               age,
		AreaName:          areaName,
		AreaType:          areaType,
		PendingUpdate:     false,
		UpdatedAt:         updatedAt,
	}, nil
}

func calculateDaysEnrolled(dateRegistered int) int {
	enrolledDay := date.ParseUnixTimestamp(int64(dateRegistered))
	diff := time.Now().Sub(enrolledDay)
	hrs := diff.Hours()
	days := int(math.Floor(hrs / 24))
	return days
}

func calculateAge(dateOfBirth DateOfBirth) int {
	year := dateOfBirth.Year
	month := dateOfBirth.Month
	day := dateOfBirth.Day

	if year == 0 {
		return 0
	}

	if month == 0 {
		month = 1
	}

	if day == 0 {
		day = 1
	}

	userBirthday := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
	diff := time.Now().Sub(userBirthday)
	days := diff.Hours() / 24
	age := int(math.Floor(days / 365))

	return age
}

func getKeyByValue(value string, category string) string {
	var jsonUrl string
	switch category {
	case "area_names":
		jsonUrl = config.Conf.UserResource.AreaNameHost
	case "area_types":
		jsonUrl = config.Conf.UserResource.AreaTypeHost
	case "product_categories":
		jsonUrl = config.Conf.UserResource.ProductCategoryHost
	}
	if jsonUrl == "" {
		return "unknown"
	}

	response, err := http.Get(jsonUrl)

	if err != nil {
		log.Println(err.Error())
		return "unknown"
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err.Error())
		return "unknown"
	}

	var jsonObj map[string]interface{}
	json.Unmarshal(responseData, &jsonObj)

	keyToValue := util.InterfaceToMapSlice(jsonObj[category])
	result := util.GetKeyByValue(keyToValue, value)

	return result
}
