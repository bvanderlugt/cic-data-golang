package domain

import (
	"strconv"
	"time"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/date"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
)

type TransactionEntity struct {
	ID                      int
	TransactionHash         string
	BlockNumber             int
	BlockTime               time.Time
	SenderAddress           string
	ReceiverAddress         string
	AmountSent              *bigint.Bigint
	AmountReceived          *bigint.Bigint
	TransactionType         string
	SenderToken             string
	ReceiverToken           string
	SenderRegistered        time.Time
	SenderProductCategory   string
	SenderGender            string
	SenderAge               int
	SenderAreaName          string
	SenderAreaType          string
	ReceiverRegistered      time.Time
	ReceiverProductCategory string
	ReceiverGender          string
	ReceiverAge             int
	ReceiverAreaName        string
	ReceiverAreaType        string
}

func (t *TransactionEntity) GetHeaders() []string {
	return []string{
		"ID",
		"TransactionHash",
		"BlockNumber",
		"BlockTime",
		"SenderAddress",
		"ReceiverAddress",
		"AmountSent",
		"AmountReceived",
		"TransactionType",
		"SenderToken",
		"ReceiverToken",
		"SenderRegistered",
		"SenderProductCategory",
		"SenderGender",
		"SenderAge",
		"SenderAreaName",
		"SenderAreaType",
		"ReceiverRegistered",
		"ReceiverProductCategory",
		"ReceiverGender",
		"ReceiverAge",
		"ReceiverAreaName",
		"ReceiverAreaType",
	}
}

func (t *TransactionEntity) ToSlice() []string {
	return []string{
		strconv.Itoa(t.ID),
		t.TransactionHash,
		strconv.Itoa(t.BlockNumber),
		date.Format(t.BlockTime),
		t.SenderAddress,
		t.ReceiverAddress,
		t.AmountSent.String(),
		t.AmountReceived.String(),
		t.TransactionType,
		t.SenderToken,
		t.ReceiverToken,
		date.Format(t.SenderRegistered),
		t.SenderProductCategory,
		t.SenderGender,
		strconv.Itoa(t.SenderAge),
		t.SenderAreaName,
		t.SenderAreaType,
		date.Format(t.ReceiverRegistered),
		t.ReceiverProductCategory,
		t.ReceiverGender,
		strconv.Itoa(t.ReceiverAge),
		t.ReceiverAreaName,
		t.ReceiverAreaType,
	}
}

func (transactionCollection *TransactionCollection) ToTransaction(sender *UserEntity, receiver *UserEntity) TransactionEntity {
	// data calculation
	transactionHash := transactionCollection.TxHash
	blockNumber := transactionCollection.BlockNumber
	blockTime := date.ParseUnixTimestamp(int64(transactionCollection.DateBlock))
	senderAddress := sender.BlockchainAddress
	receiverAddress := receiver.BlockchainAddress
	amountSent := transactionCollection.FromValue
	amountReceived := transactionCollection.ToValue
	transactionType := getTransactionType(transactionCollection)
	senderToken := transactionCollection.SourceToken
	receiverToken := transactionCollection.DestinationToken
	senderRegistered := sender.DateRegistered
	senderProductCategory := sender.ProductCategory
	senderGender := sender.Gender
	senderAge := sender.Age
	senderAreaName := sender.AreaName
	senderAreaType := sender.AreaType
	receiverRegistered := receiver.DateRegistered
	receiverProductCategory := receiver.ProductCategory
	receiverGender := receiver.Gender
	receiverAge := receiver.Age
	receiverAreaName := receiver.AreaName
	receiverAreaType := receiver.AreaType

	return TransactionEntity{
		TransactionHash:         transactionHash,
		BlockNumber:             blockNumber,
		BlockTime:               blockTime,
		SenderAddress:           senderAddress,
		ReceiverAddress:         receiverAddress,
		AmountSent:              amountSent,
		AmountReceived:          amountReceived,
		TransactionType:         transactionType,
		SenderToken:             senderToken,
		ReceiverToken:           receiverToken,
		SenderRegistered:        senderRegistered,
		SenderProductCategory:   senderProductCategory,
		SenderGender:            senderGender,
		SenderAge:               senderAge,
		SenderAreaName:          senderAreaName,
		SenderAreaType:          senderAreaType,
		ReceiverRegistered:      receiverRegistered,
		ReceiverProductCategory: receiverProductCategory,
		ReceiverGender:          receiverGender,
		ReceiverAge:             receiverAge,
		ReceiverAreaName:        receiverAreaName,
		ReceiverAreaType:        receiverAreaType,
	}
}

type Transactions []*TransactionEntity

type TransactionGetData struct {
	TransactionCollections TransactionCollections `json:"data"`
}

type TransactionCollections []*TransactionCollection

type TransactionCollection struct {
	BlockNumber      int            `json:"block_number"`
	TxHash           string         `json:"tx_hash"`
	DateBlock        float64        `json:"date_block"`
	Sender           string         `json:"sender"`
	Recipient        string         `json:"recipient"`
	FromValue        *bigint.Bigint `json:"from_value"`
	ToValue          *bigint.Bigint `json:"to_value"`
	SourceToken      string         `json:"source_token"`
	DestinationToken string         `json:"destination_token"`
	Success          bool           `json:"success"`
	TxType           string         `json:"tx_type"`
}

func getTransactionType(transactionCollection *TransactionCollection) string {
	trustAddress := config.Conf.SystemAddress.TrustAddress
	faucetAddress := config.Conf.SystemAddress.FaucetAddress

	if transactionCollection.Sender == trustAddress || transactionCollection.Sender == faucetAddress {
		return "disbursement"
	} else if transactionCollection.Recipient == trustAddress || transactionCollection.Recipient == faucetAddress {
		return "reclamation"
	} else {
		return "standard"
	}
}
