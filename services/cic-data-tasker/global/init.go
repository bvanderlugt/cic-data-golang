package global

import (
	"sync"

	"gitlab.com/grassrootseconomics/cic-data-golang/common/pg"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/config"
	"gitlab.com/grassrootseconomics/cic-data-golang/services/cic-data-tasker/internal/job"
)

var once sync.Once

func Init() {
	once.Do(func() {
		config.Init()
		pg.Init(&pg.Config{
			HOST:     config.Conf.PG.HOST,
			USER:     config.Conf.PG.USER,
			PASSWORD: config.Conf.PG.PASSWORD,
			DBName:   config.Conf.PG.DBName,
			PORT:     config.Conf.PG.PORT,
		})
		job.Init()
	})
}
