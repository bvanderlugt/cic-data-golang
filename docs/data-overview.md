# CIC Data Overview

## Data Used for Analytics (as at 2021-04-23)

### Required Data for Spreadsheet Download

The fields below are based on the [analytics script from Will](https://github.com/GrassrootsEconomics/Dashboard/blob/master/sarafu_user_db_test.py) used on the current xDAI system.

| Field Name                 | Pub/Priv | Data Source     | Data Creator | Description                                                                     |
| -------------------------- | -------- | --------------- | ------------ | ------------------------------------------------------------------------------- |
| 🚀 `blockchain_address`     | Public   | Meta            | System       | internal user ID number                                                         |
| 🚀 `first_name`             | Private  | Meta            | User         | user's first name                                                               |
| 🚀 `last_name`              | Private  | Meta            | User         | user's last name                                                                |
| 🚀 `phone`                  | Private  | Meta            | User         | user's mobile phone number                                                      |
| 🚀 `product`                | Private  | Meta            | User         | primary good or service sold by the user                                        |
| 🚀 `location`               | Private  | Meta            | User         | user's location                                                                 |
| 🚀 `user_url`               | Private  | System          | System       | hyperlink to user's details page for admins **(1)**                             |
| 🚀 `date_registered`        | Public   | Meta            | System       | date when user signed up                                                        |
| 🚀 `days_enrolled`          | Public   | System          | Derived      | number of days since user first signed up (derived from `date_registered`)      |
| 🚀 `product_category`       | Public   | System          | Derived      | [`product_category`](../mappings/product_categories.json)                       |
| 🚀 `gender`                 | Public   | Meta            | User         | user's gender                                                                   |
| 🚀 `age`                    | Public   | System          | Derived      | calculated from `date_of_birth` held for user in Meta                           |
| 🚀 `area_name`              | Public   | System          | Derived      | metalocation, could be fuzzy matched                                            |
| 🚀 `area_type`              | Public   | System          | Derived      | type of area                                                                    |
| 🔨 `account_type`           | Public   | System          | Admin        | individual or group                                                             |
| 🔨 `preferred_language`     | Public   | USSD            | User         | user's preferred language                                                       |
| 🔨 `is_market_enabled`      | Public   | USSD            | User         | user can choose to be listed in directory of traders                            |
| 🔨 `location_confidence`    | Public   | Meta/Blockchain | Derived      | user's probable location based on trading activity (1 is low, 9 is high)        |
| 🔨 `token_balance`          | Public   | Blockchain      | Derived      | user's balance as recorded at linked blockchain address                         |
| 🚀 `ovol_in`                | Public   | Blockchain      | Derived      | total number of tokens in from non-standard transactions                        |
| 🚀 `ovol_out`               | Public   | Blockchain      | Derived      | total number of tokens out from non-standard transactions                       |
| 🚀 `otxns_in`               | Public   | Blockchain      | Derived      | total number of transactions in from non-standard transactions                  |
| 🚀 `otxns_out`              | Public   | Blockchain      | Derived      | total number of transactions out from non-standard transactions                 |
| 🚀 `ounique_in`             | Public   | Blockchain      | Derived      | total number of unique transactions in from non-standard transactions           |
| 🚀 `ounique_out`            | Public   | Blockchain      | Derived      | total number of unique transactions out from non-standard transactions          |
| 🚀 `svol_in`                | Public   | Blockchain      | Derived      | total number of tokens in from standard transactions                            |
| 🚀 `svol_out`               | Public   | Blockchain      | Derived      | total number of tokens out from standard transactions                           |
| 🚀 `stxns_in`               | Public   | Blockchain      | Derived      | total number of transactions in from standard transactions                      |
| 🚀 `stxns_out`              | Public   | Blockchain      | Derived      | total number of transactions out from standard transactions                     |
| 🚀 `sunique_in`             | Public   | Blockchain      | Derived      | total number of unique transactions in from standard transactions               |
| 🚀 `sunique_out`            | Public   | Blockchain      | Derived      | total number of unique transactions out from standard transactions              |
| 🚀 `sunique_all`            | Public   | Blockchain      | Derived      | total number of users this user has transacted with                             |
| 🚀 `last_trade_out`         | Public   | Blockchain      | Derived      | date/time of the last standard transaction made out of the account              |
| 🚀 `first_trade_in_user`    | Public   | Blockchain      | Derived      | first user (sender) to pay into user's account via standard transaction **(2)** |
| 🚀 `first_trade_in_time`    | Public   | Blockchain      | Derived      | date/time of that first payment                                                 |
| 🔨 `clustering_coefficient` | Public   | Blockchain      | Derived      | https://en.wikipedia.org/wiki/Clustering_coefficient **(3)**                    |

1. Example: https://cicada.grassrootseconomics.net/#/accounts/71C7656EC7ab88b098defB751B7401B5f6d8976F
2. Use the sender's blockchain address as the identifier
3. "Basically it is looking at loops of size 3 around a user. So for instance if you have 3 trade partners - and they are all trading with each other - then you have 100% clustering. If you have 100 trade partners then having a C=100% would be a lot harder. I often look at the number of partners multiplied by that percentage to get a sense of that user. Then we can also relatively score them against each other."

### Work in Progress

| Field Name    | Pub/Priv | Data Creator | Data Location | Description (example)                            |
| ------------- | -------- | ------------ | ------------- | ------------------------------------------------ |
| `support_net` | Public   | Blockchain   | Derived       | spillover volume of spending created by user     |
| `trade_bal`   | Public   | Blockchain   | Derived       | mutual credit-style positive or negative balance |

### Deprecated Data

| Field Name                   | Pub/Priv | Data Creator | Data Location | Description (example)                                                 |
| ---------------------------- | -------- | ------------ | ------------- | --------------------------------------------------------------------- |
| `_name` / `category`         | Public   | Derived      | Meta          | DEPRECATED **(1)** - business category set by an admin (_Food/Water_) |
| `punique_x_clustering`       | n/a      | n/a          | n/a           | DEPRECATED **(2)**                                                    |
| `user_reward`                | n/a      | n/a          | n/a           | DEPRECATED **(2)**                                                    |
| `punique_group_x_clustering` | n/a      | n/a          | n/a           | DEPRECATED **(2)**                                                    |
| `group_reward    `           | n/a      | n/a          | n/a           | DEPRECATED **(2)**                                                    |
| `first_trade_in_sphone`      | n/a      | n/a          | n/a           | DEPRECATED - will just use sender blockchain address instead          |
| `last_trade_out_days`        | Public   | Blockchain   | Derived       | DEPRECATED - can count the days dynamically if needed                 |
| `user_accounts_url`          | Private  | System       | Meta          | DEPRECATED - is linked to from user's details page in CICADA          |
| `failed_pin_attempts`        | Private  | USSD         | System        | DEPRECATED - this is a CRM thing, not analytics                       |

1. _Business Category_ is to be replaced by _Product Category_ (see [`product_categories`](../mappings/product_categories.json) mappings)
2. Related to `cluster_coef` - "The rest was an attempt at creating scaled tax redistribution, which is no longer in use."

## Example Snapshot of Area Statistics

| Kisauni CIC Stats                | 2021-04-18 |
| -------------------------------- | ---------- |
| Registrations all time           | 2,709      |
| Users that have traded < 2 times | 1,824      |
| Users that have traded >=2 times | 885        |
| total CIC in circulation         | 378,477    |
| CIC trade volume                 | 120,684    |
| Number of transactions           | 6,539      |
