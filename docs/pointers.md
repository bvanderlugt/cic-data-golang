# Pointers

Pointers are used to reference data stored in meta.

| Pointer           | "Secret"                            | Literal Identifier | Data Stored                                                 |
| ----------------- | ----------------------------------- | ------------------ | ----------------------------------------------------------- |
| Phone             | `telephone_number`                  | `:cic.phone`       | Blockchain address linked to the phone number               |
| Personal Metadata | `blockchain_address` (binary value) | `:cic.person`      | Personal data for the user linked to the blockchain address |
| Metadata Tags     | `blockchain_address` (binary value) | `:cic.custom`      | Tags used for labelling accounts                            |

```
1. x = "secret"
2. y = literal identifier
3. r = sha256(x||y)

(where || is concat)
```

## Original Q & A

Q: How does/will Meta link a user to their telephone number in USSD? Is Meta the "hub" that links a user's info across USSD, the blockchain and Meta itself? IOW, how does/will Meta be able to correlate data from USSD to a specific user recorded in Meta?

A: On meta there is a mapping of phone numbers to blockchain addresses. You would query for the address using the phone number, then query for the metadata using the resulting address.
Checkout the getAccountByPhone method on line 203 of https://gitlab.com/grassrootseconomics/cic-staff-client/-/blob/spencer/accounts-search/src/app/_services/user.service.ts
After fetching the address it calls the getAccountByAddress method on line 183.

A: Since the protocol for creating the pointers is the same then as long as ussd has a phone number or a blockchain address, it can re-create said pointers and access the data in meta.

Q: Can you guys show me where the pointer protocol is defined and/or where it is used in code?

A: it's defined in code unfortunately. We need a separate document - like with the schemas - to define this in one place. Have a look at the `cic-meta-js` dep, example in `src/assets/person.ts` which uses `src/digest.ts`.
