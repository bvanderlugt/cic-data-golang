module gitlab.com/grassrootseconomics/cic-data-golang

go 1.16

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/emersion/go-vcard v0.0.0-20210521075357-3445b9171995
	github.com/ethereum/go-ethereum v1.10.8
	github.com/gin-gonic/gin v1.7.3
	github.com/go-co-op/gocron v1.7.0
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pkg/errors v0.9.1
	github.com/pkg/sftp v1.13.2
	go.uber.org/zap v1.13.0
	golang.org/x/crypto v0.0.0-20210813211128-0a44fdfbc16e
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.13
)
