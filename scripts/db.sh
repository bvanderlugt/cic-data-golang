#! /bin/sh

set -e 

PGPASSWORD=$PG_PASSWORD psql -U $PG_USER -h $PG_HOST $PG_DB_NAME -f ./scripts/db_migrations.sql
