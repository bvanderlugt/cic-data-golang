package web3

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/bigint"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/accounts"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/registry"
	"gitlab.com/grassrootseconomics/cic-data-golang/common/web3/token"
)

type BlockchainAddress string

func (b BlockchainAddress) String() string {
	return string(b)
}

type Conf struct {
	Host                   string
	Port                   string
	MetaHost               string
	MetaPort               string
	AccountRegistryAddress string
	TokenContractAddress   string
}

type UserAccountType struct {
	AccountType string `json:"account_type"`
}

type UserPreferences struct {
	PreferredLanguage string `json:"preferred_language"`
	IsMarketEnabled   bool   `json:"is_market_enabled"`
}

type Getter interface {
	Get(url string) (*http.Response, error)
}

type Web3 struct {
	getter Getter
	conf   *Conf
	client *ethclient.Client
}

func NewWeb3(httpGetter Getter, conf *Conf) (*Web3, error) {
	web3 := &Web3{
		getter: httpGetter,
		conf:   conf,
	}
	client, err := ethclient.Dial(fmt.Sprintf("http://%s:%s", web3.conf.Host, web3.conf.Port))
	if err != nil {
		return nil, fmt.Errorf("cannot connect web3: %v", err)
	}
	web3.client = client
	return web3, nil
}

func (w *Web3) GetAllAddrs() ([]*common.Address, error) {
	registryAddress := common.HexToAddress(w.conf.AccountRegistryAddress)
	registryInstance, err := registry.NewRegistry(registryAddress, w.client)
	if err != nil {
		return nil, fmt.Errorf("cannot create new registry: %v", err)
	}

	var acctReg32 [32]byte
	acctReg := []byte("AccountRegistry")
	copy(acctReg32[:], acctReg[0:15])

	// get the accounts index address
	acctsIndexAddr, err := registryInstance.AddressOf(&bind.CallOpts{}, acctReg32)
	if err != nil {
		return nil, fmt.Errorf("cannot get the accounts index address: %v", err)
	}

	fmt.Println("Accounts Index Address:", acctsIndexAddr)

	// instantiate the accounts index
	acctsIndexInstance, err := accounts.NewAccounts(acctsIndexAddr, w.client)
	if err != nil {
		return nil, fmt.Errorf("cannot instantiate the accounts index: %v", err)
	}

	// get total number of accounts
	totalAccounts, err := acctsIndexInstance.EntryCount(&bind.CallOpts{})
	if err != nil {
		return nil, fmt.Errorf("cannot get total number of accounts: %v", err)
	}

	fmt.Println("Total Accounts:", totalAccounts)

	allAccounts := make([]*common.Address, 0)
	one := big.NewInt(1)
	start := big.NewInt(0)
	for accts := new(big.Int).Set(start); accts.Cmp(totalAccounts) < 0; accts.Add(accts, one) {
		acct, err := acctsIndexInstance.Entry(&bind.CallOpts{}, accts)
		if err != nil {
			return nil, fmt.Errorf("cannot get entry: %v", err)
		}
		allAccounts = append(allAccounts, &acct)
	}

	return allAccounts, nil
}

func (w *Web3) GetUser(s BlockchainAddress) ([]byte, error) {
	key, err := toAddressKey(s.String(), ":cic.person")
	if err != nil {
		return nil, errors.Errorf("web3 getUser toAddressKey error -> %s", err)
	}

	userUrl := fmt.Sprintf("http://%s:%s/%s", w.conf.MetaHost, w.conf.MetaPort, key)

	res, err := w.getter.Get(userUrl)
	if err != nil {
		return nil, errors.Errorf("web3 GetUser error -> %s", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, errors.Errorf("web3 GetUser error -> Request Users Data Error")
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return nil, errors.Errorf("web3 GetUser error -> %s", err)
	}

	return body, nil
}

func (w *Web3) GetUserTokenBalance(s BlockchainAddress) (*bigint.Bigint, error) {
	tokenAddress := common.HexToAddress(w.conf.TokenContractAddress)
	instance, err := token.NewToken(tokenAddress, w.client)
	if err != nil {
		return nil, fmt.Errorf("cannot instantiate the token contract: %v", err)
	}

	// get balance of an address linked to contract
	address := common.HexToAddress(s.String())
	bal, err := instance.BalanceOf(&bind.CallOpts{}, address)
	if err != nil {
		return nil, fmt.Errorf("cannot get balance: %v", err)
	}

	bigintBal := bigint.NewBigint(bal)

	return bigintBal, nil
}

func (w *Web3) GetAccountType(s BlockchainAddress) (string, error) {
	key, err := toAddressKey(s.String(), ":cic.custom")
	if err != nil {
		return "", fmt.Errorf("web3 getAccountType error: %v", err)
	}

	url := fmt.Sprintf("http://%s:%s/%s", w.conf.MetaHost, w.conf.MetaPort, key)

	res, err := w.getter.Get(url)
	if err != nil {
		return "", errors.Errorf("web3 GetAccountType error -> %s", err)
	}
	defer res.Body.Close()

	// ==========================================
	// FIXME: res.StatusCode is always 404.
	// ==========================================

	// todo: struct is not sure, need to be reviewed
	var accountType UserAccountType

	if res.StatusCode == 200 {
		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			return "", errors.Errorf("web3 getAccountType error: %s", err)
		}

		err = json.Unmarshal(body, &accountType)
		if err != nil {
			return "", errors.Errorf("web3 getAccountType error: %s", err)
		}
		// todo: currently can't get data and url is 404, 404 status need to be removed after reviewed
	} else if res.StatusCode == 400 || res.StatusCode == 404 {
		accountType.AccountType = "individual"
	} else {
		return "", fmt.Errorf("cannot getAccountType type; status code: %v", res.StatusCode)
	}

	return accountType.AccountType, nil
}

func (w *Web3) GetPreferences(s BlockchainAddress) (string, bool, error) {
	key, err := toAddressKey(s.String(), ":cic.preferences")
	if err != nil {
		return "", false, fmt.Errorf("web3 getPreferences error: %v", err)
	}

	url := fmt.Sprintf("http://%s:%s/%s", w.conf.MetaHost, w.conf.MetaPort, key)

	res, err := w.getter.Get(url)
	if err != nil {
		return "", false, errors.Errorf("web3 getPreferences error: %s", err)
	}
	defer res.Body.Close()

	// ==========================================
	// FIXME: res.StatusCode is always 404.
	// ==========================================

	// todo: struct is not sure, need to be reviewed
	var preferences UserPreferences

	if res.StatusCode == 200 {
		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			return "", false, errors.Errorf("web3 getPreferences error: %s", err)
		}

		err = json.Unmarshal(body, &preferences)
		if err != nil {
			return "", false, errors.Errorf("web3 getPreferences error: %s", err)
		}
		// todo: currently can't get data and url is 404, 404 status need to be removed after reviewed
	} else if res.StatusCode == 400 || res.StatusCode == 404 {
		preferences.PreferredLanguage = "sw"
		preferences.IsMarketEnabled = false
	} else {
		return "", false, fmt.Errorf("cannot getPreferences type; status code: %v", res.StatusCode)
	}

	return preferences.PreferredLanguage, preferences.IsMarketEnabled, nil
}
