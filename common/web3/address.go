package web3

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

const RE_ADDRHEX = `^0[xX][a-fA-F0-9]{40}$`

func toAddressKey(zeroExHex string, salt string) (string, error) {
	addressBytes, err := addressToBytes(zeroExHex)
	if err != nil {
		return "", fmt.Errorf("toAddressKey: %s", err)
	}
	return mergeKey(addressBytes, []byte(salt)), nil
}

func mergeKey(a []byte, b []byte) string {
	h := sha256.New()
	h.Write(a)
	h.Write(b)
	return hex.EncodeToString(h.Sum(nil))
}

func hexStringToBytes(s string) ([]byte, error) {
	a := make([]byte, 20)
	j := 2
	for i := 0; i < len(a); i++ {
		n, err := strconv.ParseInt(s[j:j+2], 16, 10)
		if err != nil {
			return nil, fmt.Errorf("hexStringToBytes: %s", err)
		}
		a[i] = byte(n)
		j += 2
	}
	return a, nil
}

func addressToBytes(s string) ([]byte, error) {
	matched, err := regexp.MatchString(RE_ADDRHEX, s)
	if err != nil {
		return nil, err
	}
	if !matched {
		return nil, errors.New("invalid address hex")
	}

	bytes, err := hexStringToBytes(s)
	if err != nil {
		return nil, fmt.Errorf("addressToBytes: %s", err)
	}

	return bytes, err
}
