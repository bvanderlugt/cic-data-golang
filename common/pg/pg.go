package pg

import (
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

type Config struct {
	HOST     string
	USER     string
	PASSWORD string
	DBName   string
	PORT     string
}

func Init(config *Config) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=UTC", config.HOST, config.USER, config.PASSWORD, config.DBName, config.PORT)
	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Panic("error when trying to connect to PostgreSQL", err)
	}
}

func Client() *gorm.DB {
	return db
}
