package util

func InterfaceToMapSlice(inter interface{}) map[string][]string {
	obj := inter.(map[string]interface{})

	result := make(map[string][]string)

	for county, val := range obj {
		iList := val.([]interface{})
		areas := make([]string, len(iList))
		for _, val := range iList {
			areas = append(areas, val.(string))
		}
		result[county] = areas
	}

	return result
}

func GetKeyByValue(obj map[string][]string, value string) string {
	for key, list := range obj {
		for _, listItem := range list {
			if listItem == value {
				return key
			}
		}
	}

	return "unknown"
}
