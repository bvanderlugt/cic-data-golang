package date

import "time"

// RFC3339 "2006-01-02T15:04:05Z07:00"
func Format(t time.Time) string {
	return t.Format(time.RFC3339)
}

func ParseUnixTimestamp(t int64) time.Time {
	return time.Unix(t, 0)
}

func ValidateTimestamp(t int64) bool {
	if t >= 999999999 && t <= 9999999999 {
		return true
	}
	return false
}

func LastWeek() (int64, int64) {
	now := time.Now()

	sevenDaysAgo := now.AddDate(0, 0, -7)
	yesterday := now.AddDate(0, 0, -1)

	startDatetime := time.Date(sevenDaysAgo.Year(), sevenDaysAgo.Month(), sevenDaysAgo.Day(), 0, 0, 0, 0, time.UTC)
	endDatetime := time.Date(yesterday.Year(), yesterday.Month(), yesterday.Day(), 23, 59, 59, 59, time.UTC)

	return startDatetime.Unix(), endDatetime.Unix()
}
